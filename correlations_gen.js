db.createCollection('tweets_by_hashtag_and_day');
db.createCollection('tweets_by_usermention_and_day');
db.createCollection('tweeters_by_usermention_and_day');
db.createCollection('tweets_by_link_and_day');
db.createCollection('usermention_and_hashtag_by_day');
db.createCollection('url_and_hashtag_by_day');
db.createCollection('url_and_usermention_by_day');
db.createCollection('hashtags_affinity_by_day');

//----

db.tweets.mapReduce(
	function() {
        if (this.entities != undefined && this.entities.hashtags != undefined) {
			for (var i in this.entities.hashtags) { 
				var year = 0;
				var month = 0;
				var date = 0;
				
				if (this.created_at != undefined) {
					var createdAt = new Date(Date.parse(this.created_at));
					year = createdAt.getFullYear();
					month = createdAt.getMonth() + 1;
					date = createdAt.getDate();
				}
				
				key = {'hashtag': this.entities.hashtags[i].text.toLowerCase(), 'year': NumberInt(year), 
					'month': NumberInt(month), 'date': NumberInt(date)};
				var orig = 0;
				var rt = 0;
				var repl = 0;
				if (this.in_reply_to_status_id != null) {
					repl = 1;
				} else if (this.retweeted_status_id) {
					rt = 1;
				} else {
					orig = 1;
				}
				value = {'total': NumberLong(1), 'orig': NumberLong(orig), 'rt': NumberLong(rt), 'repl': NumberLong(repl)}; 
				emit(key, value);
			}
		}
	}, 
	function(key, values) {
		var reducedVal = {'total': NumberLong(0), 'orig': NumberLong(0), 'rt': NumberLong(0), 'repl': NumberLong(0)};
		for (var i in values) {
			reducedVal.total = NumberLong(reducedVal.total + values[i].total);
			reducedVal.rt = NumberLong(reducedVal.rt + values[i].rt);
			reducedVal.repl = NumberLong(reducedVal.repl + values[i].repl);
			reducedVal.orig = NumberLong(reducedVal.orig + values[i].orig);
		}
		return reducedVal;
	},
	{
		query: {inserted_at: {$gte: ISODate('2014-08-27T16:28:00-05:00').getTime(), $lt: ISODate('2014-08-27T17:37:00-05:00').getTime()}},
		out: {reduce: 'tweets_by_hashtag_and_day'}
	}
);

//----

db.tweets.mapReduce(
	function() {
        if (this.entities != undefined && this.entities.user_mentions != undefined) {
			for (var i in this.entities.user_mentions) { 
				var year = 0;
				var month = 0;
				var date = 0;
				
				if (this.created_at != undefined) {
					var createdAt = new Date(Date.parse(this.created_at));
					year = createdAt.getFullYear();
					month = createdAt.getMonth() + 1;
					date = createdAt.getDate();
				}
				
				key = {'id_str': this.entities.user_mentions[i].id_str, 
					'screen_name': this.entities.user_mentions[i].screen_name.toLowerCase(),
					'year': NumberInt(year), 
					'month': NumberInt(month), 'date': NumberInt(date)};
				var orig = 0;
				var rt = 0;
				var repl = 0;
				if (this.in_reply_to_status_id != null) {
					repl = 1;
				} else if (this.retweeted_status_id) {
					rt = 1;
				} else {
					orig = 1;
				}
				value = {'total': NumberLong(1), 'orig': NumberLong(orig), 'rt': NumberLong(rt), 'repl': NumberLong(repl)}; 
				emit(key, value);
			}
		}
	}, 
	function(key, values) {
		var reducedVal = {'total': NumberLong(0), 'orig': NumberLong(0), 'rt': NumberLong(0), 'repl': NumberLong(0)};
		for (var i in values) {
			reducedVal.total = NumberLong(reducedVal.total + values[i].total);
			reducedVal.rt = NumberLong(reducedVal.rt + values[i].rt);
			reducedVal.repl = NumberLong(reducedVal.repl + values[i].repl);
			reducedVal.orig = NumberLong(reducedVal.orig + values[i].orig);
		}
		return reducedVal;
	},
	{
		query: {inserted_at: { $gte: ISODate('2014-08-27T16:28:00-05:00').getTime(), $lt: ISODate('2014-08-27T17:37:00-05:00').getTime() }},
		out: {reduce: 'tweets_by_usermention_and_day'}
	}
);

//-----

db.tweets.mapReduce(
	function() {
        if (this.entities != undefined && this.entities.user_mentions != undefined) {
			for (var i in this.entities.user_mentions) { 
				var year = 0;
				var month = 0;
				var date = 0;
				
				if (this.created_at != undefined) {
					var createdAt = new Date(Date.parse(this.created_at));
					year = createdAt.getFullYear();
					month = createdAt.getMonth() + 1;
					date = createdAt.getDate();
				}
				
				key = {'id_str': this.entities.user_mentions[i].id_str, 
					'screen_name': this.entities.user_mentions[i].screen_name.toLowerCase(),
					'year': NumberInt(year), 
					'month': NumberInt(month), 'date': NumberInt(date)};
				var orig = 0;
				var rt = 0;
				var repl = 0;
				if (this.in_reply_to_status_id != null) {
					repl = 1;
				} else if (this.retweeted_status_id) {
					rt = 1;
				} else {
					orig = 1;
				}
				value = {'total': NumberLong(1), 'orig': NumberLong(orig), 'rt': NumberLong(rt), 'repl': NumberLong(repl)}; 
				emit(key, value);
			}
		}
	}, 
	function(key, values) {
		var reducedVal = {'total': NumberLong(0), 'orig': NumberLong(0), 'rt': NumberLong(0), 'repl': NumberLong(0)};
		for (var i in values) {
			reducedVal.total = NumberLong(reducedVal.total + values[i].total);
			reducedVal.rt = NumberLong(reducedVal.rt + values[i].rt);
			reducedVal.repl = NumberLong(reducedVal.repl + values[i].repl);
			reducedVal.orig = NumberLong(reducedVal.orig + values[i].orig);
		}
		return reducedVal;
	},
	{
		query: {inserted_at: { $gte: ISODate('2014-08-27T16:28:00-05:00').getTime(), $lt: ISODate('2014-08-27T17:37:00-05:00').getTime() }},
		out: {reduce: 'tweeters_by_usermention_and_day'}
	}
);

//-----

db.tweets.mapReduce(
	function() {
        if (this.entities != undefined && this.entities.urls != undefined) {		
			for (var i in this.entities.urls) { 
				var year = 0;
				var month = 0;
				var date = 0;
				
				if (this.created_at != undefined) {
					var createdAt = new Date(Date.parse(this.created_at));
					year = createdAt.getFullYear();
					month = createdAt.getMonth() + 1;
					date = createdAt.getDate();
				}
				
				key = {'url': this.entities.urls[i].url, 
					'year': NumberInt(year), 
					'month': NumberInt(month), 'date': NumberInt(date)};
				var orig = 0;
				var rt = 0;
				var repl = 0;
				if (this.in_reply_to_status_id != null) {
					repl = 1;
				} else if (this.retweeted_status_id) {
					rt = 1;
				} else {
					orig = 1;
				}
				value = {'total': NumberLong(1), 'orig': NumberLong(orig), 'rt': NumberLong(rt), 'repl': NumberLong(repl)}; 
				emit(key, value);
			}
		}
	}, 
	function(key, values) {
		var reducedVal = {'total': NumberLong(0), 'orig': NumberLong(0), 'rt': NumberLong(0), 'repl': NumberLong(0)};
		for (var i in values) {
			reducedVal.total = NumberLong(reducedVal.total + values[i].total);
			reducedVal.rt = NumberLong(reducedVal.rt + values[i].rt);
			reducedVal.repl = NumberLong(reducedVal.repl + values[i].repl);
			reducedVal.orig = NumberLong(reducedVal.orig + values[i].orig);
		}
		return reducedVal;
	},
	{
		query: {inserted_at: { $gte: ISODate('2014-08-27T16:28:00-05:00').getTime(), $lt: ISODate('2014-08-27T17:37:00-05:00').getTime() }},
		out: {reduce: 'tweets_by_link_and_day'}
	}
);

//----

db.tweets.mapReduce(
    function() {
        if (this.entities != undefined && this.entities.user_mentions != undefined && 
            this.entities.hashtags != undefined) {
            for (var i in this.entities.user_mentions) { 
                for (var j in this.entities.hashtags) { 
                    var year = 0;
                    var month = 0;
                    var date = 0;
                    
                    if (this.created_at != undefined) {
                        var createdAt = new Date(Date.parse(this.created_at));
                        year = createdAt.getFullYear();
                        month = createdAt.getMonth() + 1;
                        date = createdAt.getDate();
                    }
                    
                    key = {'id_str': this.entities.user_mentions[i].id_str,
                        'screen_name': this.entities.user_mentions[i].screen_name.toLowerCase(),
                        'hashtag': this.entities.hashtags[j].text.toLowerCase(), 
                        'year': NumberInt(year), 
						'month': NumberInt(month), 'date': NumberInt(date)};
                    var orig = 0;
					var rt = 0;
					var repl = 0;
					if (this.in_reply_to_status_id != null) {
						repl = 1;
					} else if (this.retweeted_status_id) {
						rt = 1;
					} else {
						orig = 1;
					}
					value = {'total': NumberLong(1), 'orig': NumberLong(orig), 'rt': NumberLong(rt), 'repl': NumberLong(repl)}; 
                    emit(key, value);        
                }    
            }
        }
    }, 
    function(key, values) {
		var reducedVal = {'total': NumberLong(0), 'orig': NumberLong(0), 'rt': NumberLong(0), 'repl': NumberLong(0)};
		for (var i in values) {
			reducedVal.total = NumberLong(reducedVal.total + values[i].total);
			reducedVal.rt = NumberLong(reducedVal.rt + values[i].rt);
			reducedVal.repl = NumberLong(reducedVal.repl + values[i].repl);
			reducedVal.orig = NumberLong(reducedVal.orig + values[i].orig);
		}
		return reducedVal;
    },
    {
    	query: {inserted_at: { $gte: ISODate('2014-08-27T16:28:00-05:00').getTime(), $lt: ISODate('2014-08-27T17:37:00-05:00').getTime() }},
    	out: {reduce: 'usermention_and_hashtag_by_day'}
    }
);

//----

db.tweets.mapReduce(
    function() {
        if (this.entities != undefined && this.entities.user_mentions != undefined && 
            this.entities.hashtags != undefined) {
            for (var i in this.entities.urls) { 
                for (var j in this.entities.hashtags) { 
                    var year = 0;
                    var month = 0;
                    var date = 0;
                    
                    if (this.created_at != undefined) {
                        var createdAt = new Date(Date.parse(this.created_at));
                        year = createdAt.getFullYear();
                        month = createdAt.getMonth() + 1;
                        date = createdAt.getDate();
                    }
                    
                    key = {'url': this.entities.urls[i].url,
                        'hashtag': this.entities.hashtags[j].text.toLowerCase(), 
                        'year': NumberInt(year), 
						'month': NumberInt(month), 'date': NumberInt(date)};
                    var orig = 0;
					var rt = 0;
					var repl = 0;
					if (this.in_reply_to_status_id != null) {
						repl = 1;
					} else if (this.retweeted_status_id) {
						rt = 1;
					} else {
						orig = 1;
					}
					value = {'total': NumberLong(1), 'orig': NumberLong(orig), 'rt': NumberLong(rt), 'repl': NumberLong(repl)}; 
                    emit(key, value);        
                }    
            }
        }
    }, 
    function(key, values) {
		var reducedVal = {'total': NumberLong(0), 'orig': NumberLong(0), 'rt': NumberLong(0), 'repl': NumberLong(0)};
		for (var i in values) {
			reducedVal.total = NumberLong(reducedVal.total + values[i].total);
			reducedVal.rt = NumberLong(reducedVal.rt + values[i].rt);
			reducedVal.repl = NumberLong(reducedVal.repl + values[i].repl);
			reducedVal.orig = NumberLong(reducedVal.orig + values[i].orig);
		}
		return reducedVal;
    },
    {
    	query: {inserted_at: { $gte: ISODate('2014-08-27T16:28:00-05:00').getTime(), $lt: ISODate('2014-08-27T17:37:00-05:00').getTime() }},
    	out: {reduce: 'url_and_hashtag_by_day'}
    }
);

//-----

db.tweets.mapReduce(
    function() {
        if (this.entities != undefined && this.entities.user_mentions != undefined && 
            this.entities.user_mentions != undefined) {
            for (var i in this.entities.urls) { 
                for (var j in this.entities.user_mentions) { 
                    var year = 0;
                    var month = 0;
                    var date = 0;
                    
                    if (this.created_at != undefined) {
                        var createdAt = new Date(Date.parse(this.created_at));
                        year = createdAt.getFullYear();
                        month = createdAt.getMonth() + 1;
                        date = createdAt.getDate();
                    }
                    
                    key = {'url': this.entities.urls[i].url,
                        'id_str': this.entities.user_mentions[j].id_str,
                        'screen_name': this.entities.user_mentions[j].screen_name.toLowerCase(), 
                        'year': NumberInt(year), 
						'month': NumberInt(month), 'date': NumberInt(date)};
                    var orig = 0;
					var rt = 0;
					var repl = 0;
					if (this.in_reply_to_status_id != null) {
						repl = 1;
					} else if (this.retweeted_status_id) {
						rt = 1;
					} else {
						orig = 1;
					}
					value = {'total': NumberLong(1), 'orig': NumberLong(orig), 'rt': NumberLong(rt), 'repl': NumberLong(repl)}; 
                    emit(key, value);        
                }    
            }
        }
    }, 
    function(key, values) {
		var reducedVal = {'total': NumberLong(0), 'orig': NumberLong(0), 'rt': NumberLong(0), 'repl': NumberLong(0)};
		for (var i in values) {
			reducedVal.total = NumberLong(reducedVal.total + values[i].total);
			reducedVal.rt = NumberLong(reducedVal.rt + values[i].rt);
			reducedVal.repl = NumberLong(reducedVal.repl + values[i].repl);
			reducedVal.orig = NumberLong(reducedVal.orig + values[i].orig);
		}
		return reducedVal;
    },
    {
    	query: {inserted_at: { $gte: ISODate('2014-08-27T16:28:00-05:00').getTime(), $lt: ISODate('2014-08-27T17:37:00-05:00').getTime() }},
    	out: {reduce: 'url_and_usermention_by_day'}
    }
);

//----

db.tweets.mapReduce(
    function() {
        if (this.entities != undefined && this.entities.hashtags != undefined) {
            var hashtags_sorted = [].concat(this.entities.hashtags);
            
            hashtags_sorted.sort(function(hashtag_a, hashtag_b)
            {
                var ret = hashtag_a.text < hashtag_b.text ? -1 : hashtag_a.text > hashtag_b.text ? 1 : 0;
                return ret;
            });             
            
            for (var i in hashtags_sorted) { 
                if (i == hashtags_sorted.length - 1) {
                    continue;
                }
                
                for (var j in hashtags_sorted.slice(i+1)) {
                    if (hashtags_sorted[i] === hashtags_sorted[j]) {
                        continue;
                    }
                    
                    var year = 0;
                    var month = 0;
                    var date = 0;
                    
                    if (this.created_at != undefined) {
                        var createdAt = new Date(Date.parse(this.created_at));
                        year = createdAt.getFullYear();
                        month = createdAt.getMonth() + 1;
                        date = createdAt.getDate();
                    }
                    
                    key = {'hashtag_1': hashtags_sorted[i].text.toLowerCase(),
                        'hashtag_2': hashtags_sorted[j].text.toLowerCase(), 
                        'year': NumberInt(year), 
						'month': NumberInt(month), 'date': NumberInt(date)};
                    var orig = 0;
					var rt = 0;
					var repl = 0;
					if (this.in_reply_to_status_id != null) {
						repl = 1;
					} else if (this.retweeted_status_id) {
						rt = 1;
					} else {
						orig = 1;
					}
					value = {'total': NumberLong(1), 'orig': NumberLong(orig), 'rt': NumberLong(rt), 'repl': NumberLong(repl)}; 
                    emit(key, value);        
                }    
            }
        }
    }, 
    function(key, values) {
		var reducedVal = {'total': NumberLong(0), 'orig': NumberLong(0), 'rt': NumberLong(0), 'repl': NumberLong(0)};
		for (var i in values) {
			reducedVal.total = NumberLong(reducedVal.total + values[i].total);
			reducedVal.rt = NumberLong(reducedVal.rt + values[i].rt);
			reducedVal.repl = NumberLong(reducedVal.repl + values[i].repl);
			reducedVal.orig = NumberLong(reducedVal.orig + values[i].orig);
		}
		return reducedVal;
    },
    {
    	query: {inserted_at: { $gte: ISODate('2014-08-27T16:28:00-05:00').getTime(), $lt: ISODate('2014-08-27T17:37:00-05:00').getTime() }},
    	out: {reduce: 'hashtags_affinity_by_day'}
    }
);